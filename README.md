**Generate Tests for Gherkin feature files.**

This application creates skeleton Java files (Tests and Runners) for automated selenium tests. For each .feature file the application will create a corresponding test and runner java classes.

### Usage:
To build, use standard mvn commands (e.g `mvn clean install`). This will create the jar place it in your local M2 repo.

### To use in your Selenium project:
Add the following to your pom.xml file.

```xml
<profiles>
    <profile>
      <id>genCukes</id>
      <activation>
        <property>
          <name>genCukes</name>
        </property>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>com.digitalplatoon</groupId>
            <artifactId>CucumberTestGenerator</artifactId>
            <version>1.0-SNAPSHOT</version>
            <configuration>
              <featureTestsLocation>src/test/java/com/eoh/seleniumtraining/login</featureTestsLocation>
              <featureTestsPackage>com.eoh.seleniumtraining.login</featureTestsPackage>
              <gherkinScriptsLocation>src/main/resources/features</gherkinScriptsLocation>
            </configuration>
            <executions>
              <execution>
                <id>generate-cucumber-tests</id>
                <phase>package</phase>
                <goals>
                  <goal>generate</goal>
                </goals>
              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
```
  
  
Run with "-DgenCukes"  e.g "mvn clean install -DgenCukes".

Using the above profile in your pom.xml file, assuming you have a Something.feature in /src/main/resources, a LoginFeatureTes.java and LoginFeatureTestRunner will be created in test package com.eoh.seleniumtraining.login.

```
Feature: Test the login functionality on the Armory application

Scenario: The user should be able to login to the application with correct credentials
Given user is on the login page
When user enters correct username as password and selects login
Then user gets logged in confirmation
```



```java
package com.eoh.seleniumtraining.login;

import static junit.framework.Assert.assertTrue;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;

public class SomethingFeatureTest  {


  @Given("^user is on the login page$")
  public void userIsOnTheLoginPage () {

  }

  @When("^user enters correct username as password and selects login$")
  public void userEntersCorrectUsernameAsPasswordAndSelectsLogin () {

  }

  @Then("^user gets logged in confirmation$")
  public void userGetsLoggedInConfirmation () {

  }

}

```



```java
package com.eoh.seleniumtraining.login;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import com.eoh.seleniumtraining.Config;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes= Config.class)
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/main/resources/features", glue = "com.eoh.seleniumtraining.login", plugin = { "pretty" })
public class SomethingFeatureRunnerTest{

}


```


  

  