package com.digitalplatoon.generator;

import java.io.Serializable;

public class GherkinLineType implements Serializable {

  private String annotationValue;
  private String methodName;

  public GherkinLineType(String t) {
    this.annotationValue = "^" + t + "$";
    String[] methodNameValues = t.split("\\s+");
    StringBuilder sb = new StringBuilder();
    sb.append(methodNameValues[0]);
    for (int i = 1; i < methodNameValues.length; i++) {
      sb.append(StringUtils.makeFirstLetterUppercase(methodNameValues[i]));
    }
    this.methodName = sb.toString();
  }

  public GherkinLineType() {
  }

  /**
   * @return the annotationValue
   */
  public String getAnnotationValue() {
    return annotationValue;
  }

  /**
   * @param annotationValue the annotationValue to set
   */
  public void setAnnotationValue(String annotationValue) {
    this.annotationValue = annotationValue;
  }

  /**
   * @return the methodName
   */
  public String getMethodName() {
    return methodName;
  }

  /**
   * @param methodName the methodName to set
   */
  public void setMethodName(String methodName) {
    this.methodName = methodName;
  }

}
