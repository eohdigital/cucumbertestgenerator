package com.digitalplatoon.generator;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import lombok.Data;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

@Data
/**
 * Goal that creates features tests from gherkin scripts (.feature files)
 *
 * @goal generate
 *
 * @phase process-sources
 */
public class FeatureTestGenerator extends AbstractMojo {

  private static final String FEATURE_TEST_POSTFIX = "Test.java";

  /**
   * Location of where the feature test files will be generated
   *
   * @parameter expression="${testPackage}"
   * @required
   */
  private String featureTestsLocation;

  /**
   * Location of where the feature test files will be generated
   *
   * @parameter expression="${featuresLocation}"
   * @required
   */
  private String gherkinScriptsLocation;

  /**
   * package of the tests
   *
   * @parameter expression="${featureTestsPackage}"
   * @required
   */
  private String featureTestsPackage;

  /**
   * package of the Config.java file
   *
   * @parameter expression="${configPackage}"
   * @required
   */
  private String configPackage;

  private List<GherkinLineType> givens;
  private List<GherkinLineType> whens;
  private List<GherkinLineType> thens;

  public void execute() throws MojoExecutionException {
    File inputSource = new File(gherkinScriptsLocation);
    for (String gherkinFeatureFile : inputSource.list()) {
      if (gherkinFeatureFile.endsWith("feature")) {

        File javaFile = new File(featureTestsLocation, createFeatureTestFileName(gherkinFeatureFile));
        File javaRunnerFile = new File(featureTestsLocation, createFeatureRunnerTestFileName(gherkinFeatureFile));
        File gherkinFeature = new File(gherkinScriptsLocation, gherkinFeatureFile);
        if (javaFile.exists()) {
          System.out.println("Feature Test " + javaFile.getName() + " already exists. Skipping generation.");
        } else {
          generateFeatureTest(gherkinFeature, javaFile);
        }

        if (javaRunnerFile.exists()) {
          System.out.println("Feature Test Runner" + javaFile.getName() + " already exists. Skipping generation.");
        } else {
          generateFeatureRunnerTest(gherkinFeature, javaRunnerFile);
        }

      }
    }
  }

  private String createFeatureTestFileName(String gherkinFeatureFile) {
    String[] fileNameParts = gherkinFeatureFile.split("\\.");
    StringBuilder featureTestFileName = new StringBuilder(fileNameParts[0]);;
    featureTestFileName.append(StringUtils.makeFirstLetterUppercase(fileNameParts[1]));
    featureTestFileName.append(FEATURE_TEST_POSTFIX);
    return featureTestFileName.toString();

  }

  private String createFeatureRunnerTestFileName(String gherkinFeatureFile) {
    String[] fileNameParts = gherkinFeatureFile.split("\\.");
    StringBuilder featureTestFileName = new StringBuilder(fileNameParts[0]);;
    featureTestFileName.append(StringUtils.makeFirstLetterUppercase(fileNameParts[1]));
    featureTestFileName.append("Runner");
    featureTestFileName.append(FEATURE_TEST_POSTFIX);
    return featureTestFileName.toString();

  }

  private void generateFeatureTest(File gherkinFeature, File file) throws MojoExecutionException {

    try {

      Configuration cfg = new Configuration(new Version(2, 3, 23));
      cfg.setClassForTemplateLoading(FeatureTestGenerator.class, "/templates");

      cfg.setDefaultEncoding("UTF-8");
      cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

      Map<String, Object> input = new HashMap<String, Object>();

      input.put("featureTestsPackage", featureTestsPackage);
      input.put("configPackage", configPackage);
      input.put("featureTestJavaClassName", file.getName().replace(".java", ""));

      generateBody(gherkinFeature);

      input.put("gs", givens);
      input.put("ws", whens);
      input.put("ts", thens);

      // 2.2. Get the template
      Template template = cfg.getTemplate("FeatureTest.ftl");

      // For the sake of example, also write output into a file:
      Writer fileWriter = new FileWriter(file);
      try {
        template.process(input, fileWriter);
      } finally {
        fileWriter.close();
      }
    } catch (TemplateException | IOException ex) {
      throw new MojoExecutionException(ex.getMessage());
    }
  }

  private void generateFeatureRunnerTest(File gherkinFeature, File file) throws MojoExecutionException {

    try {

      Configuration cfg = new Configuration(new Version(2, 3, 23));
      cfg.setClassForTemplateLoading(FeatureTestGenerator.class, "/templates");

      cfg.setDefaultEncoding("UTF-8");
      cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

      Map<String, Object> input = new HashMap<String, Object>();

      input.put("featureTestsPackage", featureTestsPackage);
      input.put("gherkinScriptsLocation", gherkinScriptsLocation);
      input.put("featureTestsPackage", featureTestsPackage);
      input.put("configPackage", configPackage);
      input.put("featureRunnerTestJavaClassName", file.getName().replace(".java", ""));

      // 2.2. Get the template
      Template template = cfg.getTemplate("FeatureRunnerTest.ftl");

      // For the sake of example, also write output into a file:
      Writer fileWriter = new FileWriter(file);
      try {
        template.process(input, fileWriter);
      } finally {
        fileWriter.close();
      }
    } catch (TemplateException | IOException ex) {
      throw new MojoExecutionException(ex.getMessage());
    }
  }

  private void generateBody(File gherkinFeatureFile) {
    givens = new ArrayList<GherkinLineType>();
    whens = new ArrayList<GherkinLineType>();
    thens = new ArrayList<GherkinLineType>();

    BufferedReader bReader = null;

    try {
      bReader = new BufferedReader(new FileReader(gherkinFeatureFile));
      String line = "";

      while ((line = bReader.readLine()) != null) {
        if (line.startsWith("Given")) {
          givens.add(new GherkinLineType(line.substring(6)));
        }

        if (line.startsWith("When")) {
          whens.add(new GherkinLineType(line.substring(5)));
        }

        if (line.startsWith("Then")) {
          thens.add(new GherkinLineType(line.substring(5)));
        }
      }
    } catch (FileNotFoundException ex) {
      Logger.getLogger(FeatureTestGenerator.class.getName()).log(Level.SEVERE, null, ex);
    } catch (IOException ex) {
      Logger.getLogger(FeatureTestGenerator.class.getName()).log(Level.SEVERE, null, ex);
    } finally {
      try {
        bReader.close();
      } catch (IOException ex) {
        Logger.getLogger(FeatureTestGenerator.class.getName()).log(Level.SEVERE, null, ex);
      }
    }

  }
}
