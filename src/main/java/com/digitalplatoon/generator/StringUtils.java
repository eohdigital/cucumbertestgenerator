package com.digitalplatoon.generator;

public class StringUtils {

  public static String makeFirstLetterUppercase(String s) {
    String firstLetter = s.substring(0, 1);
    return s.replaceFirst(firstLetter, firstLetter.toUpperCase());
  }



}
