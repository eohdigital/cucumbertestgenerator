package ${featureTestsPackage};

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import ${configPackage}.Config;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes= Config.class)
@RunWith(Cucumber.class)
@CucumberOptions(features = "${gherkinScriptsLocation}", glue = "${featureTestsPackage}", plugin = { "pretty" })
public class ${featureRunnerTestJavaClassName}{

}
