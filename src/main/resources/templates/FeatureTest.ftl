package ${featureTestsPackage};

import static junit.framework.Assert.assertTrue;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;

public class ${featureTestJavaClassName}  {


  <#list gs as g>
  @Given("${g.annotationValue}")
  public void ${g.methodName} () {

  }
  </#list>

  <#list ws as w>
  @When("${w.annotationValue}")
  public void ${w.methodName} () {

  }
  </#list>

  <#list ts as t>
  @Then("${t.annotationValue}")
  public void ${t.methodName} () {

  }
  </#list>

}

